package com.youmata.stock;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.youmata.stock.entity.Produit;
import com.youmata.stock.entity.Role;
import com.youmata.stock.entity.User;
import com.youmata.stock.repositories.ProduitRepository;
import com.youmata.stock.repositories.RoleRepository;
import com.youmata.stock.repositories.UserRepository;
import com.youmata.stock.util.RoleEnum;

@SpringBootApplication
public class ServeurGestionStockApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(ServeurGestionStockApplication.class, args);

//		ProduitRepository produitRepository = ctx.getBean(ProduitRepository.class);
//
//		produitRepository.save(new Produit("Livre", 50, 20));
//		produitRepository.save(new Produit("Cahier", 200, 5.25f));
//		produitRepository.save(new Produit("Stylo", 500, 2.10f));
//
//		RoleRepository roleRepository = ctx.getBean(RoleRepository.class);
//
//		Role roleUser = new Role(RoleEnum.ROLE_USER);
//		Role roleAdmin = new Role(RoleEnum.ROLE_ADMIN);
//
//		roleRepository.save(roleUser);
//		roleRepository.save(roleAdmin);
//
//		UserRepository userRepository = ctx.getBean(UserRepository.class);
//
//		User user = new User("user", "password1", true);
//		user.setRoles(Arrays.asList(roleUser));
//
//		userRepository.save(user);
//
//		User admin = new User("admin", "password2", true);
//		admin.setRoles(Arrays.asList(roleUser, roleAdmin));
//
//		userRepository.save(admin);
	}
}
