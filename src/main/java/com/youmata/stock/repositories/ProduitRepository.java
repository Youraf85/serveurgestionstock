package com.youmata.stock.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.youmata.stock.entity.Produit;

@Repository
public interface ProduitRepository extends JpaRepository<Produit, Long>{

}