package com.youmata.stock.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.youmata.stock.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{

}
