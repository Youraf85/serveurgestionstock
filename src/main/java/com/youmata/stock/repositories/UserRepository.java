package com.youmata.stock.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.youmata.stock.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByUsername(String str);
}
