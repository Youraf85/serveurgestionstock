package com.youmata.stock.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.youmata.stock.entity.Produit;
import com.youmata.stock.repositories.ProduitRepository;
import com.youmata.stock.service.ICrudService;

@Service
@Primary
public class ProduitServiceImpl implements ICrudService<Produit, Long>{

	@Autowired
	private ProduitRepository produitRepository;
	
	@Override
	public List<Produit> getAll() {
		return produitRepository.findAll();
	}

	@Override
	public void add(Produit produit) {
		produitRepository.save(produit);
	}

	@Override
	public void update(Produit produit) {
		produitRepository.save(produit);
	}

	@Override
	public void delete(Long id) {
		Produit produit = new Produit();
		produit.setId(id);
		produitRepository.delete(produit);
	}
	
	@Override
	public void saveAll(Iterable<Produit> iterable) {
		produitRepository.saveAll(iterable);	
	}
}
